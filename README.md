Family Study Password
==================

THE biblical game for family study.

Famil Study Password (FSP) is a simple to use, fun game utilizing biblical terms to create a fun family study game, utilizing the rules of the game <a href="http://en.wikipedia.org/wiki/Password_%28game_show%29">Password</a> 

FSP utilizes Python 3, with the GUI module PySide, the python bindings for Qt.

Dead Simple And Easy To Use GUI
===============================

<center>
<a href="http://imgur.com/QBZSb3W"><img src="http://i.imgur.com/QBZSb3Wl.png" title="Hosted by imgur.com"/></a>

<a href="http://imgur.com/y9pQ8LB"><img src="http://i.imgur.com/y9pQ8LBl.png" title="Hosted by imgur.com"/></a>
</center>

How To Add Words To The List
============================

1. Open game_list.py
2. Add any words you'd like in pythonic-list form
3. Save the file, and it will now be integrated into the game
4. Please feel free to push the changes to the list to the main repo, and if the words are good, and aren't repetative, I will merge the changes.
