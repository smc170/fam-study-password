"""
Family Study Password - The Biblical Game
   
Copyright (C) 2013 Spencer Caesare

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


#!/usr/bin/env python3

import sys
from sys import exit
from PySide import QtCore, QtGui
from random import *

from game_gui import Ui_main_window
from game_list import cards


class game_window(QtGui.QWidget, Ui_main_window):
	def __init__(self, parent=None):
		super(game_window, self).__init__(parent)
		
		self.setupUi(self)
	
		self.loop_count = 0

		self.random_word()
		
		
	def random_word(self):
		
		if self.loop_count >= 2:
			self.get_button.clicked.disconnect(self.random_word)
		else:
			pass

		self.card_to_play = choice(cards)
		cards.remove(self.card_to_play)

		password_label = self.password_label
		get_button = self.get_button
		
		self.password_label.setText('Push Button To Receive Word')
		self.get_button.setText('Push Me To Get A Word')

		self.loop_count += 1
		
		self.get_button.clicked.connect(self.set_labels)



	def set_labels(self):
		self.password_label.setText(self.card_to_play)

		self.get_button.setText('Push To Clear Word')
		
		self.get_button.clicked.disconnect(self.set_labels)
		self.get_button.clicked.connect(self.random_word)
		
		if not cards:
			self.password_label.setText("Congrats! You've gone through all the words! Press the button to quit.")
			self.get_button.setText('Push Me To Quit')
			self.get_button.clicked.connect(QtCore.QCoreApplication.instance().quit)
		else:
			pass
		
		

if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	window = game_window()
	window.show()
	sys.exit(app.exec_())
	
