# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'password_game.ui'
#
# Created: Mon May 20 18:42:36 2013
#      by: pyside-uic 0.2.14 running on PySide 1.1.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_main_window(object):
    def setupUi(self, main_window):
        main_window.setObjectName("main_window")
        main_window.resize(609, 565)
        main_window.setMinimumSize(QtCore.QSize(500, 500))
        self.gridLayout = QtGui.QGridLayout(main_window)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.password_label = QtGui.QLabel(main_window)
        font = QtGui.QFont()
        font.setPointSize(50)
        font.setWeight(75)
        font.setItalic(True)
        font.setUnderline(False)
        font.setBold(True)
        self.password_label.setFont(font)
        self.password_label.setScaledContents(False)
        self.password_label.setAlignment(QtCore.Qt.AlignCenter)
        self.password_label.setWordWrap(True)
        self.password_label.setObjectName("password_label")
        self.verticalLayout.addWidget(self.password_label)
        self.get_button = QtGui.QPushButton(main_window)
        font = QtGui.QFont()
        font.setPointSize(25)
        self.get_button.setFont(font)
        self.get_button.setObjectName("get_button")
        self.verticalLayout.addWidget(self.get_button)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(main_window)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def retranslateUi(self, main_window):
        main_window.setWindowTitle(QtGui.QApplication.translate("main_window", "Family Study Password", None, QtGui.QApplication.UnicodeUTF8))
        self.password_label.setText(QtGui.QApplication.translate("main_window", "Push Button To Receive Word", None, QtGui.QApplication.UnicodeUTF8))
        self.get_button.setText(QtGui.QApplication.translate("main_window", "Push Me To Get A Word", None, QtGui.QApplication.UnicodeUTF8))

